//
//  ViewController.swift
//  DispatchOnceExample
//
//  Created by Zakk Hoyt on 4/25/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBAction func onceButtonTouchUpInside(_ sender: Any) {
        // Token is inferred from file, function, line
        DispatchQueue.once {
            print("This should only print one time")
        }
    }
    
    
    
    @IBAction func onceQueueButtonTouchUpInside(_ sender: Any) {
        // Create a queue with a label from 0-9
        let dispatchQueue = DispatchQueue(label: "\(UInt.random)",
            qos: .default,
            attributes: .concurrent,
            autoreleaseFrequency: .never,
            target: nil)
        
        // Dispatch once using queue label as token
        let token = dispatchQueue.label
        dispatchQueue.async {
            dispatchQueue.once(token: token, block: {
                print("This should once for each global queue: \(token)")
            })
        }
    }
}

extension UInt {
    static var random: UInt {
        return UInt(arc4random_uniform(10))
    }
}
