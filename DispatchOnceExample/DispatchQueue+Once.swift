//
//  DispatchQueue+Once.swift
//
//  Based on Tod Cunningham's answer from https://stackoverflow.com/questions/37886994/dispatch-once-after-the-swift-3-gcd-api-changes/39983813#39983813
//
//    Executes a block of code, associated with a unique token, only once.
//    The code is thread safe and will only execute the code once even in
//    the presence of multithreaded calls.
//
//    - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
//    - parameter block: Block to execute once

import Foundation

public extension DispatchQueue {
    
    // MARK: - Private variables
    
    private static var _onceTracker: [String] = []
    
    // MARK: - Public class methods
    
    public class func once(file: String = #file,
                           function: String = #function,
                           line: Int = #line,
                           block: () -> Void) {
        let token = "\(file):\(function):\(String(line))"
        once(token: token, block: block)
    }

    public class func once(token: String,
                           block: () -> Void) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if _onceTracker.contains(token) { return }
        _onceTracker.append(token)
        block()
    }

    // MARK: Public - instance methods
    
    public func once(file: String = #file,
                           function: String = #function,
                           line: Int = #line,
                           block: () -> Void) {
        DispatchQueue.once(file: file,
                           function: function,
                           line: line,
                           block: block)
    }
    
    public func once(token: String,
                           block: () -> Void) {
        DispatchQueue.once(token: token,
                           block: block)
    }
}
